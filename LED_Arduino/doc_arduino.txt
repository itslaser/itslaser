Dokumentation für Arduino Programm


Verwendete Bibliotheken:

IRLib2 - https://github.com/cyborg5/IRLib2


Zu Beachten:
Programm verwendet zum Empfangen die Interrupt Pins des Arduinos: https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/

Protokoll:
    Über das IR-Signal werden diverse Daten übermittelt:
        - Spieler ID des Feuernden
        - Treffer Flag
        - ID des Sensors der getroffen wurde

        |_ _ _ _|_|_ _ _ _ _ _ _ _|
            |    |      |
        SensorID |      |
        (4 bit)  |      |
                 |      |
        Treffer Flag    |
        (1 bit)         |       
                    Spieler ID
                    (8 bit)

    Diese Zahl wird in ein integer umgewandelt und über die IR-Schnittstelle gesendet.

    Beispiel:

    Spieler ID =    15;
    Treffer Flag =  True;
    Sensor ID =     5;

    In binärer Darstellung (jeweils so viele bits wie im Protokoll definiert. Mit Nullen vorne aufgefüllt):

    Spieler ID =    00001111;
    Treffer Flag=   1
    Sensor ID=      0101

    Nach Protokoll zusammengesetzt:

    Sensor ID | Treffer Flag | Spieler ID
    0101      | 1            | 00001111

    Nachricht=          0101100001111;
    Nachricht als Int=  2831;

    Diese Zahl wird als ASCII gesendet. Diese Nachricht kann mit dem PYthon Modul "messagesFunkctions" 
    wieder in die seperaten Informationen zerlegt werden. Dokumentation dafür folgt.



Funktion des Programms:

definition
    (1)     Einbinden der IR-Bibliothek
    (2)     Debug Flag um Fehlermeldungen in die serielle Schnittstelle zu schreiben.
    (5-10)  Definieren der Variablen
    (12-14) initialisieren von Sender, Empfänger und Decoder

chartoint
    Funktion um ASCII Zeichen (charArray) der seriellen Schnittstelle wieder in Integers umzuwandeln.
    Dabei wird jede Zahl im Array ausgelesen und dem richtigen Exponenten des Zehnersystems zugeordnet (29-31).
    Ausgegeben wird ein unsigned integer mit 32bit (17).

setup
    (43-45) Die serielle Verbindung wird initialisiert und der Empfänger aktiviert (51)

loop
    (57)        Sobald eine Nachricht auf der seriellen Schnittstelle verfügbar ist wird der Code ausgeführt
    (58-69)     Sollte eine legitime Nachricht enthalten sein, wird sie in den Sendebuffer geschrieben. Falls nicht, wir der Buffer geleert
    (71-89)     Sobal ein "Carriage Return" in der Nachricht enthalten ist wird der Inhalt davor zu einem integer umgewandelt und gesendet. 
                Der Empfänger muss nach dem Empfangen oder Senden einer Nachricht jedes Mal wieder gestartet werden
    (92-96)     Der Buffer wird wieder geleert und der Counter zurückgesetzt um eine neue Nachricht schreiben zu können

    (109)       Sobald eine Nachricht auf dem Empfänger erkannt wird, wird der Code ausgeführt
    (110)       ID als option für mehrere Empfänger. Derzeit noch nicht implementiert. Alle Empfänger müssen auf den gleichen Pin geschaltet werden
    (116)       Nachricht wird decodiert
    (118)       Nachricht wird ausgelesen
    (120-125)   Sollte eine gültige Nachricht erkannt worden sein (int < 512) dann wird Trefferbit gesetzt und die Information an den Pi über die serielle Schnittstelle gesendet
    (127)       Der Empfänger wird wie beschrieben wieder aktiviert