#include <IRLibAll.h>

#define DEBUG

//int ledPin = 3;             //Pin für LED
int recvPin = 2;            //Pin für Empfänger
int ledFreq = 56;           //Trägerfrequenz in kHz
char sendbuffer[8] ={0};    //Initialisiere Sendebuffer mit 8 Bytes und jeweils Inhalt 0
int sendcounter = 0;        //Initialisiere Sendecounter
int baudRate = 9600;        //Baud Rate für serielle Verbindung

IRsendNEC mySender;             //Initialsiiere Sender
IRrecv myReceiver(recvPin);     //Initialsiiere Empfänger
IRdecode myDecoder;             //Initialsiiere Decoder

//Funktionsdefinition für ASCII zu Integer
uint32_t chartoint(char* charArray) {

    uint32_t summe = 0;
    int exp = 1;
    int i = 0;

    while (isDigit(charArray[i])) {     //Prüft bis zu welcher Stelle eine Zahl im charArray vorkommt
        i++;
    }


    while (i > 0) {
        uint32_t currentDigit = (uint32_t)charArray[i-1] - 48;  //Konvertierung von ASCII zu Int
        currentDigit = currentDigit * exp;                      //Umrechnung ins Zehnersystem
        exp = exp * 10;

        summe = summe + currentDigit;                           
        i--;
    }


    return summe;
}


void setup() {
    Serial.begin(baudRate);
    delay(2000);
    while(!Serial);
    
    #ifdef DEBUG
    Serial.println("Ready!");
    #endif
    
    myReceiver.enableIRIn(); // Start the receiver
}

void loop() {

    //Send Data when Serial Data is received
    if (Serial.available()) {               //Warte auf Serielle kommunikation
        int16_t data = Serial.read();       //Lese von serieller Schnittstelle
        if(data >= 0){                          
            if(sendcounter < 8){
                sendbuffer[sendcounter] = (uint8_t)data;
                sendcounter ++;
            }else{
                //Code sollte nicht ausgeführt werden wenn serielle kommunikation richtig
                 for(int i = 0; i< 8;i++){
                  sendbuffer[i]=0;
                }
                sendcounter = 0;
            }

            //Wenn letztes gelesenes Zeichen == 13, dann sende Nachricht
            if(sendbuffer[sendcounter-1] == 13 ){
                //Konvertiere char zu int
                int interpret = chartoint(sendbuffer);

                #ifdef DEBUG
                    Serial.print("Sending: ");
                    for(int i = 0; i < sendcounter-1; i++){
                        Serial.print((sendbuffer[i]));
                    }
                    Serial.print(" - ");
                    Serial.print((int)interpret);
                    Serial.println();
                #endif
                
                if (interpret < 30000 && interpret > 0) {           //Prüfe auf gültiges Integer
                    mySender.send((uint32_t)interpret, ledFreq);    //Send data
                    myReceiver.enableIRIn();                        // Start the receiver again

                    #ifdef DEBUG
                    pinMode(LED_BUILTIN, OUTPUT);
                    digitalWrite(LED_BUILTIN, HIGH);
                    delay(1000);
                    digitalWrite(LED_BUILTIN, LOW);
                    #endif
                }
                //Reset buffer
                for(int i = 0; i < 8; i++){
                  sendbuffer[i] = 0;
                }
                sendcounter = 0;
            }    
        }
    }

    //Read IR Data
    if (myReceiver.getResults()) { 
        int ID = 1;
        
        #ifdef DEBUG
        Serial.println("Empfange...");
        #endif

        myDecoder.decode();

        uint32_t received = myDecoder.value;

        if (received < 512) {
            received = received + 512;

            Serial.print("a");
            Serial.print(received);
            Serial.println();
        }
        myReceiver.enableIRIn(); // Start the receiver again
    }
}