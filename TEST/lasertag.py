flag_ser = 1
flag_drive = 0
flag_pantilt = 0
flag_ctrlr = 1


import messages
import serial
import xbox
import bewegung
import time
if flag_pantilt:
    import pantilthat
import filters


comPort = "/dev/ttyACM0"
baudRate = 9600
submit = False
playerId = 3
enemyID = 0

serialInputBuffer = ""
serialMess = ""


# initialisiere Serielle
# Establish the connection on a specific port
ser = serial.Serial(comPort, baudRate)

# initialisiere Controller
joy = xbox.Joystick()

old_ticks = time.time()

# endlosschleife
while True:

    enemyID = 0

    # Netzwerk empfangen

    # Get
    # Data
    # from
    # App


    while ser.inWaiting() > 0:  # Wartet auf serielle Nachricht

        # liest 1 Byte aus Nachricht
        buffer = str(ser.read(1), encoding='ascii')
        serialInputBuffer = serialInputBuffer + \
            buffer  # fügt Nachricht dem Buffer hinzu

        if len(serialInputBuffer) >= 2:

            if (serialInputBuffer[-2] == "\r") and (serialInputBuffer[-1] == "\n"):

                # fängt erste Statusnachricht von Arduino ab
                if (serialInputBuffer[0:-2] == "Ready!") or (serialInputBuffer[0:-2] == "Empfange..."):

                    print(serialInputBuffer[:-2])
                    serialInputBuffer = ""

                # sobald Nachricht vollständig empfangen
                elif (serialInputBuffer[0] == "a"):

                    mess = int(serialInputBuffer[1:-2])
                    serialMess = messages.parse(mess)
                    enemyID = serialMess[0]
                    print("Treffer von: " + str(enemyID))
                    serialInputBuffer = ""

                else:

                    serialInputBuffer = ""



#        interpretiere nachricht


#          Fahren
    if flag_ctrlr:

        x, y = joy.leftStick()
        r_bumper = joy.rightBumper()
        l_bumper = joy.leftBumper()
        l_trigger = joy.leftTrigger()
        r_trigger = joy.rightTrigger()
        

    if flag_drive:

        if (x < 0.1) and (x > -0.1):
            x = 0

        if (y < 0.1) and (y > -0.1):
            y = 0

        if l_trigger > 0:
            bewegung.vorwärtslinks()
            bewegung.rückwärtsrechts()
            bewegung.speed(l_trigger)

        elif r_trigger > 0:
            bewegung.vorwärtsrechts()
            bewegung.rückwärtslinks()
            bewegung.speed(r_trigger)

        else:
            bewegung.Richtung(x, y)
            bewegung.Geschwindigkeitlinks(x, y)
            bewegung.Geschwindigkeitrechts(x, y)

    ####
    # DEBUG
    ###

    #print("Y: " + str(round(y,2)) + " X: " + str(round(x,2)) + " R: " + str(r_bumper) + " L: " + str(l_bumper))

    # Kopfbewegung

    if flag_pantilt:
        # rechten Stick auslesen
        x_r, y_r = joy.rightStick()
        # Werte mappen und Richtung umkehren
        x_r = x_r * -90
        y_r = y_r * -90
        print("Pan: " + str(round(x_r, 2)) + "Tilt: " + str(round(y_r, 2)))

        # Werte glätten
        #x_r = filters.lowpass(x_r)
        #y_r = filters.lowpass(y_r)
        # Kopf bewegen
        pantilthat.pan(x_r)
        pantilthat.tilt(y_r)

    ticks = time.time()

    if joy.A():
        
        if (ticks - old_ticks > 0.3):
                
                ser.write(bytes(messages.generate(playerId, 0), 'utf-8'))
                ser.write(bytes("\r", encoding='utf-8'))
                print(messages.generate(playerId, 0))
                playerId = playerId + 1
                old_ticks = time.time()
                

    # Netzwerk senden

        # Send
        # IR
        # Data
        # to
        # App

    #time.sleep(0.2)
