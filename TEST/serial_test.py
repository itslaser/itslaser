import serial
import messages

comPort = "/dev/ttyACM0"
baudRate = 9600
submit = False
playerId = 3

serialInputBuffer = ""
serialMess = ""

# initialisiere Serielle
ser = serial.Serial(comPort, baudRate) # Establish the connection on a specific port

while True:

    while ser.inWaiting() > 0:  #Wartet auf serielle Nachricht

        buffer = str(ser.read(1), encoding='ascii')         #liest 1 Byte aus Nachricht
        serialInputBuffer = serialInputBuffer + buffer      #fügt Nachricht dem Buffer hinzu

        if len(serialInputBuffer) >= 2:

            if (serialInputBuffer[-2] == "\r") and (serialInputBuffer[-1] == "\n"):
                
                if (serialInputBuffer[0:-2] == "Ready!") or (serialInputBuffer[0:-2] == "Empfange...") :    #fängt erste Statusnachricht von Arduino ab

                    print(serialInputBuffer[:-2])
                    serialInputBuffer = ""

                elif (serialInputBuffer[0] == "a"):         #sobald Nachricht vollständig empfangen

                    mess = int(serialInputBuffer[1:-2])
                    serialMess = messages.parse(mess)
                    enemyID = serialMess[0]
                    print("Treffer von: " + str(enemyID))
                    serialInputBuffer = ""

                else:

                    serialInputBuffer = ""