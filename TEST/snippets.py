import random
from time import sleep


for i in range(400):
    print("\r" + str(i), end="")
    sleep(0.01) 


    ######

import sys

def replace_cmd_line(output):
    """Replace the last command line output with the given output."""
    sys.stdout.write(output + '\r')
    sys.stdout.flush()

    ######

sudo pip3 install pyfiglet

from pyfiglet import Figlet

f = Figlet(font='starwars')
print f.renderText('ITS LASER')



Setting UDEV rule
From terminal: lsusb
Look for Arduino or FTDI (if prior Arduino Uno)
Type the two 4 digit IDs: they are like 093a:2510
093a is the vendor ID
2510 is the product ID
Now, navigate your terminal to  /etc/udev/rules.d/
As root, create a file:
The name must be a two digit number (choose a big number like 99), followed by a hyphen, a name and .rules; for example "99-arduino.rules"
Inside the file, write the following, where XXXX is the product ID and YYYY the vendor ID:
For Uno:
SUBSYSTEMS=="usb", ATTRS{idProduct}=="XXXX", ATTRS{idVendor}=="YYYY", SYMLINK+="ttyACM0"
From terminal: udevadm control --reload-rules
Finally, reboot your system.

##### ffmpeg commands

```bash
ffmpeg -i fullhd.avi -preset ultrafast -vcodec libx264 -tune zerolatency -f mpegts udp://192.168.87.51:1234
ffmpeg -i fullhd.avi -c:v h264_omx -an -b:v 3000k -f mpegts udp://192.168.87.51:1234
```
